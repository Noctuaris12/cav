# -*- coding: utf-8 -*-
"""
userInput.py

Created on Mon Nov 23 09:51:55 2020

@author: Caroline 
"""

import cv2 
import sys

def userInput(image, logger):
    """
    This function asks the user to select most and least weathered pixels.
    
    image (np.array) : input image
    logger : logs handler
        
    Return [(Rect2d),(Rect2d)] : bounding boxes for most and least weathered pixels 
    """
    
    # Most weathered pixels
    logger.info("Please select most weathered pixels and press enter.")
    mostROI = cv2.selectROI(image) # opencv method that opens image and permits selection
    if mostROI!=(0,0,0,0): 
        logger.info("Selection done.")
    else :
        logger.error("Error : pixels not selected")
        cv2.destroyAllWindows() # prevent freeze
        sys.exit(1)
        
    cv2.destroyAllWindows() # prevent freeze

    # Least weathered pixels
    logger.info("Please select least weathered pixels and press enter.")
    leastROI = cv2.selectROI(image)
    if leastROI!=(0,0,0,0):
        logger.info("Selection done.")
    else :
        logger.error("Error : pixels not selected")
        cv2.destroyAllWindows() # prevent freeze
        sys.exit(1)
        
    cv2.destroyAllWindows() # prevent freeze
        
    return [mostROI, leastROI]

