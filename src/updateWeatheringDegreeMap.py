# -*- coding: utf-8 -*-
"""
updateWeatheringDegreeMap.py

Created on Mon Dec 7 08:27:58 2020

@author: Caroline 
"""

import cv2 
from numpy import newaxis

def updateDegreeMap(degreeMap, segmentation, nbIterations) :
    """
    This function updates the weathering degree map.
    
    degreeMap (np.array) : image of the degree map
    segmentation (np.array) : image of segmented degree map with binary values 0 and 1
    nbIterations (int) : number of iterations
        
    Return (np.array) : image of the updated degree map 
    """
    
    degreeMapOld = degreeMap.copy()
    degreeMapUpdated = degreeMap.copy()
    
    # Values from paper
    ks = 0.025
    kw = 1.0
    
    # Compute segmentation mask : update only if segmentation==0
    segmentation_norm = cv2.normalize(segmentation, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8UC1)
    mask = ~segmentation_norm # inverse
    mask[mask==255] = 1
    
    # Compute iterations to update the degree map
    while (nbIterations != 0) :
        P = cv2.medianBlur(degreeMapOld, 3) * mask # compute the average P where segmentation==0, so where mask==1
        P = P[:, :, newaxis] # reshape to 3d array for compilation
        degreeMapUpdated = degreeMapOld + (P * ks * kw) # formula : D' += P * ks * kw
        degreeMapOld = degreeMapUpdated.copy()
        nbIterations = nbIterations - 1
        
    # Check the boundaries
    degreeMapUpdated[degreeMapUpdated > 1.0] = 1.0
    degreeMapUpdated[degreeMapUpdated < 0.01] = 0.01
        
    return degreeMapUpdated




    ### V1 : First version -> Not optimized
    
    # size_x = degreeMap.shape[0]
    # size_y = degreeMap.shape[1]
    
    # while (nbIterations != 0) :
    #     print(nbIterations)
    #     for i in range(1, size_x-1) :
    #         for j in range(1, size_y-1) :
    #             if (segmentation[i][j]==0) :
    #                 P = (degreeMapOld[i-1][j] + degreeMapOld[i][j-1] + degreeMapOld[i+1][j] + degreeMapOld[i][j+1]) / 4.0
    #                 degreeMapUpdated[i][j] = degreeMapOld[i][j] + P * ks * kw
                    
    #                 if (degreeMapUpdated[i][j] > 1.0) :
    #                     degreeMapUpdated[i][j] = 1.0;
    #                 elif (degreeMapUpdated[i][j] < 0.01) :
    #                     degreeMapUpdated[i][j] = 0.01;
         
    #     degreeMapOld = degreeMapUpdated.copy()
    #     nbIterations = nbIterations - 1

