# -*- coding: utf-8 -*-
"""
computeWeatheringDegreeMap.py

Created on Mon Nov 23 15:30:53 2020

@author: Caroline
"""

import cv2 as cv
import numpy as np
from numpy.linalg import norm 
from scipy.optimize import nnls

#====================================================================

# UTILITIES

# def phi(r) :
#     return np.exp(-r**2)

def f(L, a, b, x, y) :
    sigma_a = 0.2
    sigma_s = 50.0
    features = [L/sigma_a, a/sigma_a, b/sigma_a, x/sigma_s, y/sigma_s]
    feature_vector = np.array(features)
    return feature_vector

def normalize_coordinates(i, j, image) :
    size_i, size_j = image.shape[:2]
    x = i/(size_i - 1.)
    y = j/(size_j - 1.)
    return x, y

def normalize_lab(i, j, image) :
    pixel = image[i][j]
    L = pixel[0] / 255.0
    a = pixel[1] / 255.0
    b = pixel[2] / 255.0
    return L, a, b

#====================================================================

def makeOmegaSet(image,ROI):
    """
    This function makes a set of pixels from user input.
    
    image (np.array) : input image
    ROI ([(Rect2d), (Rect2d)]) : bounding boxes for most and least weathered pixels 
        
    Return (list) : omega [(value, x, y, di), ...], a list of pixels from ROI (value and coordinates) and an associated value (most (di = 1.0) and least (di = 0.01) weathered pixels)
    """
    
    omega = []
        
    mostROI = ROI[0]
    leastROI = ROI[1]

    # Most weathered pixels
    bbox_ori_x = mostROI[1]
    bbox_ori_y = mostROI[0]
    if (mostROI[3] < 30) : 
        bbox_size_x = mostROI[3]
    else :
        # keep a few pixels if the user selects a big bbox, for a quicker computation
        bbox_size_x = 30
    if (mostROI[2] < 30) :
        bbox_size_y = mostROI[2]
    else :
        bbox_size_y = 30

    for i in range(bbox_ori_x, bbox_ori_x + bbox_size_x + 1) :
        for j in range(bbox_ori_y, bbox_ori_y + bbox_size_y + 1) :
            pixel = (image[i][j], i, j, 1.0) # di = 1.0 if weathered
            omega.append(pixel)
            
    # Least weathered pixels
    bbox_ori_x = leastROI[1]
    bbox_ori_y = leastROI[0]
    if (leastROI[3] < 30) :
        bbox_size_x = leastROI[3]
    else :
        bbox_size_x = 30
    if (leastROI[2] < 30) :
        bbox_size_y = leastROI[2]
    else :
        bbox_size_y = 30

    for i in range(bbox_ori_x, bbox_ori_x + bbox_size_x + 1) :
        for j in range(bbox_ori_y, bbox_ori_y + bbox_size_y + 1) :
            pixel = (image[i][j], i, j, 0.01) # di = 0.01 if non-weathered
            omega.append(pixel)
        
    return omega

#====================================================================

def computeDegreeMap(omega, image, logger) :
    """
    This function solves the optimization problem, eq. (1) from the paper, then computes the weathering degree of each pixel, eq. (2).

    omega (list) : list of (pixel value (Lab colorspace), coord x, coord y, associated value di)
    image (np.array) : input image
    logger : logs handler
        
    Return (np.array) : degree map image
    """
    
    # Prepare data from omega set
    L = [] # luminance L
    a = [] # chrominance a
    b = [] # chrominance b
    x = [] # normalized coordinates x
    y = [] # normalized coordinates y
    d = [] # associated weathered value d (1.0 or 0.01)
    
    for pixel in omega :
        Li,ai,bi = normalize_lab(pixel[1], pixel[2], image)
        L.append(Li)
        a.append(ai)
        b.append(bi)
        i,j = normalize_coordinates(pixel[1], pixel[2], image)
        x.append(i)
        y.append(j)
        d.append(pixel[3])
        
    # Prepare the matrix M, containing f vectors for Omega set
    M = []
    for i in range(0, len(omega)) :
        fi = f(L[i],a[i],b[i],x[i],y[i])
        M.append(fi)
    M = np.array(M)
    
    # Prepare the matrix A 
    A_list = []
    for i in range(0,len(omega)) :
        #print(i)
        fi = M[i,:]
        value_list = []
        for j in range(0,len(omega)) :
            fj = M[j,:]
            diff = np.subtract(fi,fj)
            norm_diff = norm(diff,2)
            phi = np.exp(- np.power(norm_diff,2))
            value_list.append(phi)
        value_vector = np.array(value_list) 
        A_list.append([value_vector])
    A_matrix = np.array(A_list) 
    A_matrix = np.squeeze(A_matrix)
    
    # Prepare the vector b
    b_list = []
    for i in range(0,len(omega)) :
        b_list.append(d[i])
    b_vector = np.array(b_list) 
    
    # Solve the least square problem || Ax - b ||²
    logger.info("... Solving least square problem ...")
    alpha = nnls(A_matrix, b_vector)
    
    # Compute Degree Map : eq. (2)
    logger.info("... Computing weathering degrees ...")
    
    size_x = image.shape[0]
    size_y = image.shape[1]
    
    degreeMap = np.ones((size_x,size_y,1),np.float32)
    
    for i in range(0, size_x) :
        for j in range(0, size_y) :
            norm_L,norm_a,norm_b = normalize_lab(i, j, image)
            norm_x,norm_y = normalize_coordinates(i, j, image)
            fp = f(norm_L, norm_a, norm_b, norm_x, norm_y)
            ### V1 : First version -> Not optimized
            # value = 0
            # for k in range(0, len(omega)) :
            #     fk = M[k,:]
            #     diff = np.subtract(fp,fk)
            #     norm_diff = norm(diff,2)
            #     phi = np.exp(- np.power(norm_diff,2))
            #     value = value + alpha[0][k]*phi
            #     degreeMap[i][j] = float(value)
            fp_temp_array = np.broadcast_to(fp,(len(omega),len(fp))) # matrice len(Omega) x len(fp) => on répète le vecteur fp Omega fois
            diff = np.power(fp_temp_array-M,2) # matrice de differences terme à terme len(Omega) x len(fp)
            somme_par_col = np.sum(diff,axis=1)
            norm_diff = np.sqrt(somme_par_col)
            phi = np.exp(- np.power(norm_diff,2))
            degreeMap[i][j] = np.dot(phi,alpha[0])
    
    return degreeMap
     
#====================================================================
    
def computeSegmentation(degreeMap, image_name) :
    """
    This function segments the Degree Map with a threshold, adapted to the input image if in our database.

    degreeMap (np.array) : Degree Map image
    image_name (str) : the name of the input image
        
    Return (np.array) : segmentation of the Degree Map
    """
    
    if (image_name == 'moss'):
        threshold_value = 0.6
    elif (image_name == 'forest') :
        threshold_value = 0.9
    else :
        threshold_value = 0.5
        
    useless,segmentation = cv.threshold(degreeMap, threshold_value, 1, cv.THRESH_BINARY)
    
    return segmentation