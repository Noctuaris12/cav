# -*- coding: utf-8 -*-
"""
computeShadingMap.py

Created on Sat Dec 26 15:37:36 2020

@author: Bastien 
"""

import cv2
import numpy as np

def computeShadingMapTest(image: np.array, weatheringMap: np.array, weatheringMapSegmented: np.array):
    """
    This functions allows to compute a shadowmap of a given image given its weathering degree

    """
    #-----------------------------------------------------------------------------------------------
    #
    # 20 class segmentation according to weathering degree & average class luminance computation
    #
    #-----------------------------------------------------------------------------------------------
    
    #array containing both the luminance and weathering degree values for every non weathered pixel
    nonWeatheredCount = np.count_nonzero(weatheringMapSegmented[:,:] == 0)
    nonWeatheredPixelData = [(0, 0) for i in range(nonWeatheredCount)]
    
    #counter
    nonWeatheredIndex = 0
    
    #computhing luminance of every pixel in the image and storing it into an array
    Luminance, a ,b = cv2.split(cv2.cvtColor(image, cv2.COLOR_BGR2Lab));
    Luminance = Luminance/255
    
    #storing the luminance & weathering degree of every pixels
    for i in range(len(image)):
        for j in range(len(image[0])):
            pixelLuminance = Luminance[i][j]
            pixelWeatheringDegree = weatheringMap[i][j]
            if(weatheringMapSegmented[i][j] == 0):
                nonWeatheredPixelData[nonWeatheredIndex] = (pixelLuminance, pixelWeatheringDegree)
                nonWeatheredIndex = nonWeatheredIndex + 1
    
    #sorting pixel luminances value according to their weathering degree to generate the 20 classes that allow for segmentation later
    nonWeatheredPixelData.sort(key=lambda x:x[1])
    
    #array storing the bounds of every classes
    nonWeatheredPixelBounds = [10.0 for i in range(20)]
    
    #we compute the upper bounds of each classes to create the shadowmap
    for i in range(20):
        (lum, weathering) = nonWeatheredPixelData[int(len(nonWeatheredPixelData)/21*(i+1))]
        nonWeatheredPixelBounds[i] = weathering
    (lum, weathering) = nonWeatheredPixelData[len(nonWeatheredPixelData)-1]
    nonWeatheredPixelBounds[19] = weathering
    
    #we compute the average luminance per class for the non weathered pixels
    averageLuminancePerClasses = [1.0 for i in range(20)]
    currentAverage = 0
    currentCounter = 0
    currentClass = 0
    
    for i in range(len(nonWeatheredPixelData)):
        (pixelLuminance, pixelWeatheringDegree) = nonWeatheredPixelData[i]
        #if the current pixel is in the next class we compute the average of the previous class and we start accumulating values for the next class
        if(pixelWeatheringDegree > nonWeatheredPixelBounds[currentClass]):
            averageLuminancePerClasses[currentClass] = (currentAverage/currentCounter)
            currentAverage = 0
            currentCounter = 0
            currentClass = currentClass + 1
        currentAverage = currentAverage + pixelLuminance
        currentCounter = currentCounter + 1
    
    #-----------------------------------------------------------------------------------------------
    #
    # Final shadowmap computation
    #
    #-----------------------------------------------------------------------------------------------
    
    #we compute the final shadowmap
    shadowMapWeathered = np.zeros((len(image), len(image[0]), 1), np.float32)
    shadowMapNonWeathered = np.zeros((len(image), len(image[0]), 1), np.float32)
    
    highestNonWeatheredValue = 0.0
    
    for i in range(len(image)):
        for j in range(len(image[0])):
            pixelLuminance = Luminance[i][j]
            pixelWeatheringDegree = weatheringMap[i][j]
            if(weatheringMapSegmented[i][j] == 0.0):
                pixelClass = 0
                for k in range(20):
                    if(pixelWeatheringDegree > nonWeatheredPixelBounds[k]):
                        continue
                    else:
                        pixelClass = k
                        break;
                nonWeatheredShadowValue = pixelLuminance / averageLuminancePerClasses[pixelClass]
                if nonWeatheredShadowValue > 1.0 :
                    nonWeatheredShadowValue = 1.0
                shadowMapNonWeathered[i][j][0] = nonWeatheredShadowValue
            else:
                shadowMapWeathered[i][j][0] = pixelLuminance
    
    #cv2.imshow("Weathered", shadowMapWeathered)
    #cv2.imshow("Non Weathered", shadowMapNonWeathered)
    #cv2.imshow("Both", shadowMapNonWeathered + shadowMapWeathered)
    #print("press escape to close window")
    #cv2.waitKey(0)
    #cv2.destroyAllWindows()
    
    return shadowMapNonWeathered + shadowMapWeathered
    
    