# -*- coding: utf-8 -*-
"""
quilting.py

from : https://github.com/rohitrango/Image-Quilting-for-Texture-Synthesis
"""

import os
import numpy as np
import argparse
import cv2
from matplotlib import pyplot as plt
from src.quilting_utils.preprocess import *
from src.quilting_utils.generate import *
from math import ceil

#====================================================================

def quiltingFromPath(imagePath: str, outH: int, outW: int, blockSize: int = 20, overlap: int = 1.0/6.0, plot: int = 1, tolerance: float = 0.1):
    """
    This function allows to generate upscaled textures from an exemplar.

    imagePath: path to the examplar used to generate final texture
    outH: output image height
    outW: output image width
    blockSize: sizes of the generated blocks of textures
    overlap: block overlapping relative to block size
    plot: plot type used
    tolerance: parameter describing the overlapping tolerance for blocks
        
    Return : upscale texture using texture quilting
    """
    
    # Get all blocks
    image = cv2.imread(imagePath)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)/255.0
    print("Image size: ({}, {})".format(*image.shape[:2]))
    return quilting(image, outH, outW, blockSize, overlap, plot, tolerance)
    
 #====================================================================
   
def quilting(image: np.array, outH: int, outW: int, blockSize: int = 20, overlap: int = 1.0/6.0, plot: int = 1, tolerance: float = 0.1):
    """
    This function allows to generate upscaled textures from an exemplar.

    image: examplar used to generate final texture
    outH: output image height
    outW: output image width
    blockSize: sizes of the generated blocks of textures
    overlap: block overlapping relative to block size
    plot: plot type used
    tolerance: parameter describing the overlapping tolerance for blocks
        
    Return : upscale texture using texture quilting
    """
    
    # Start the main loop here
    # print("Using plot {}".format(plot))
    # Set overlap to 1/6th of block size
    if overlap > 0:
        overlap = int(blockSize*overlap)
    else:
        overlap = int(blockSize/6.0)
    
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)/255.0

    quited_h, quilted_w = outH, outW
    quilted = generateTextureMap(image, blockSize, overlap, quited_h, quilted_w, tolerance)
    quilted = (255*quilted).astype(np.uint8)
    quilted = cv2.cvtColor(quilted, cv2.COLOR_RGB2BGR)
    resizedQuilted = cv2.resize(quilted, (outW, outH))
    
    return resizedQuilted
    