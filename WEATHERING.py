# -*- coding: utf-8 -*-
"""
Single Image Weathering via Exemplar Propagation

Created on Mon Nov 23 08:39:18 2020

@author: Caroline 
"""

import cv2 as cv
import numpy as np
import logging
from colorlog import ColoredFormatter
import typer
import os

# Functions
from src import *
from src.userInput import *
from src.computeWeatheringDegreeMap import *
from src.computeExemplar import *
from src.quilting import *
from src.computeShadingMap import *
from src.updateWeatheringDegreeMap import *

#====================================================================
# Initialisation : Packaging Typer
#====================================================================

app = typer.Typer()

@app.command()
def Welcome():
    print("Welcome to a Weathering Implementation !")
    # This is for compiling in Spyder, Typer does not understand where there is only one command.
    return 0

#====================================================================
# Initialisation : Format of logs
#====================================================================

LOG_LEVEL = logging.DEBUG
LOGFORMAT = "%(log_color)s%(asctime)s%(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
#LOGFORMAT = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"

logging.root.setLevel(LOG_LEVEL)
formatter = ColoredFormatter(
	LOGFORMAT,
	datefmt='%d/%m/%Y %H:%M:%S ',
	reset=True,
	log_colors={
		'DEBUG':    'bold_cyan',
		'INFO':     'bold_white',
		'WARNING':  'bold_yellow',
		'ERROR':    'bold_red',
		'CRITICAL': 'bold_red,bg_white',
	},
	secondary_log_colors={},
	style='%'
)
stream = logging.StreamHandler()
stream.setLevel(LOG_LEVEL)
stream.setFormatter(formatter)
logger = logging.getLogger('pythonConfig')
logger.setLevel(LOG_LEVEL)

if (logger.hasHandlers()):
    logger.handlers.clear()
logger.addHandler(stream)


#====================================================================
#====================================================================
# WEATHERING
#====================================================================
#====================================================================

@app.command()
def Weathering(path: str, nbiterations: int):
    
    # Read input image
    #path = "./input/moss.jpg"
    image = cv.imread(path)
    
    # Prepare image name and size for later
    path_split = path.split("/")
    size_split = len(path_split)
    image_name_full = path_split[size_split-1] # ex : moss.jpg
    name_split = image_name_full.split(".")
    image_name = name_split[0] # ex : moss
    size_x, size_y = image.shape[:2]
    
    #====================================================================
    # (a) User Input
    # The user picks pixel from most weathered region and from non-weathered region.
    #====================================================================

    logger.info("(a) Asking for User Input.")
    
    # The user selects most weathered region and least weathered region
    ROI = userInput(image,logger)
    
    
    #====================================================================
    # (b) Weathering Degree Map
    # We compute a weathered degree map based on Radial Basis Functions (RBFs) 
    # to handle wide color distributions over objects.
    #====================================================================
    
    logger.info("(b) Computing Weathering Degree Map ...")
    
    # Convert from RGB to Lab colorspace 
    image_lab = cv.cvtColor(image, cv.COLOR_BGR2LAB)
    
    # Prepare omega set : a set of pixels within the user-specified scribbles.
    omega = makeOmegaSet(image_lab, ROI)
    
    # Solve optimization problem : (eq. 1 from paper) + Compute the degree of each pixel : (eq. 2 from paper)
    degreeMap = computeDegreeMap(omega, image_lab, logger)
    
    # Write images
    cv.imwrite('./result/{}/{}/degreeMap.png'.format(image_name,nbiterations), degreeMap)
    degreeMapDisplay = cv.normalize(degreeMap, None, alpha=0, beta=255, norm_type=cv.NORM_MINMAX, dtype=cv.CV_8UC1)
    cv.imwrite('./result/{}/{}/degreeMapDisplay.png'.format(image_name,nbiterations), degreeMapDisplay)
    
    # Segment the image with a threshold
    segmentation = computeSegmentation(degreeMap, image_name)
    
    # Write images
    cv.imwrite('./result/{}/{}/segmentation.png'.format(image_name,nbiterations), segmentation)
    segmentationDisplay = cv.normalize(segmentation, None, alpha=0, beta=255, norm_type=cv.NORM_MINMAX, dtype=cv.CV_8UC1)
    cv.imwrite('./result/{}/{}/segmentationDisplay.png'.format(image_name,nbiterations), segmentationDisplay)
    
    logger.info("Weathering Degree Map done.")
    
    
    #====================================================================
    # (c) Weathering Exemplar
    # We compute an exemplar and we upscale it into a texture.
    #====================================================================
    
    logger.info("(c) Computing Weathering Exemplar and Texture ...")
    
    if (os.path.isfile("./result/{}/texture.png".format(image_name))):
        logger.info("Texture already computed.")
        texture = cv.imread("./result/{}/texture.png".format(image_name))
    else :
        # Compute the exemplar by asking the user to select most weathered area
        exemplar = computeExemplar(image, logger)
            
        # Upscale the exemplar into a texture by quilting
        logger.info("... Computing Texture (please wait a few minute) ...")
        texture = quilting(exemplar, size_x, size_y)
            
        # Write images
        cv.imwrite('./result/{}/texture.png'.format(image_name), texture)
    
    logger.info("Texture done.")
    
    
    #====================================================================
    # (d) Shading
    # We compute a shading map :
    #====================================================================
    
    logger.info("(d) Computing Shadow Map ...")
    
    # Compute the shadow map using the image,the weathering degree map and the binary segmented degree map
    shadowMap = computeShadingMapTest(image, degreeMap, segmentation)
    
    # Write images
    cv.imwrite('./result/{}/{}/shadowMap.png'.format(image_name,nbiterations), shadowMap)
    shadowMapDisplay = cv.normalize(shadowMap, None, alpha=0, beta=255, norm_type=cv.NORM_MINMAX, dtype=cv.CV_8UC1)
    cv.imwrite('./result/{}/{}/shadowMapDisplay.png'.format(image_name,nbiterations), shadowMapDisplay)
    
    logger.info("Shadow Map Generated.")
    
    
    #====================================================================
    # (e) Updated Weathering Degree Map
    # Once both are computed, we update the weathering degree map.
    #====================================================================
    
    logger.info("(e) Updating the Weathering Degree Map ...")
    
    degreeMapUpdated = updateDegreeMap(degreeMap, segmentation, nbiterations)
    
    # Write images
    cv.imwrite('./result/{}/{}/degreeMapUpdated.png'.format(image_name,nbiterations), degreeMapUpdated)
    degreeMapUpdatedDisplay = cv.normalize(degreeMapUpdated, None, alpha=0, beta=255, norm_type=cv.NORM_MINMAX, dtype=cv.CV_8UC1)
    cv.imwrite('./result/{}/{}/degreeMapUpdatedDisplay.png'.format(image_name,nbiterations), degreeMapUpdatedDisplay)
    
    logger.info("Degree Map updated.")
    
    
    #====================================================================
    # (f) Output
    # We spread patches from the exemplar to input image according to new 
    # weathering degree map and shadow map
    #====================================================================
    
    logger.info("(f) Computing the final output ...")
    
    ### Prepare elements for computation (eq. (5) and (6) from the paper)
    
    # Prepare Input Image I
    image = np.float32(image) / 255  
    image_lab = cv.cvtColor(image, cv.COLOR_BGR2LAB)
    # cv.imwrite('./result/{}/{}/imageLAB.png'.format(image_name,nbiterations), image_lab)
    I_L, I_A, I_B = cv.split(image_lab)

    I_L = I_L / 100
    I_L = I_L[:,:, np.newaxis]
    I_A = (I_A + 128) / 255
    I_A = I_A[:,:, np.newaxis]
    I_B = (I_B + 128) / 255
    I_B = I_B[:,:, np.newaxis]

    # Prepare Weathering Texture Z
    texture = np.float32(texture) / 255
    texture_lab = cv.cvtColor(texture, cv.COLOR_BGR2LAB)
    
    Z_L, Z_A, Z_B = cv.split(texture_lab)
    Z_L = Z_L / 100
    Z_L = Z_L[:,:, np.newaxis]
    Z_A = (Z_A + 128) / 255
    Z_A = Z_A[:,:, np.newaxis]
    Z_B = (Z_B + 128) / 255
    Z_B = Z_B[:,:, np.newaxis]
    
    # Shading Map S
    S = shadowMap.copy()
    
    # Updated Degree Map D : D' = D if D > sigma, else D = 0
    D = degreeMapUpdated.copy()
    sigma = 0.7 # cf paper
    D[D <= sigma] = 0
    
    one_minus_d = np.subtract(1.0, D)
    
    I_lum = I_L
    I_chromaA = I_A
    I_chromaB = I_B
    
    # Eq. (5) and (6) from the paper    
    for i in range(0, size_x) :
        for j in range(0, size_y) :
            if (segmentation[i][j]==0) :
                I_lum[i][j] = (D[i][j]*Z_L[i][j] + (one_minus_d[i][j])*I_L[i][j]) * S[i][j]
                I_chromaA[i][j] = D[i][j]*Z_A[i][j] + (one_minus_d[i][j])*I_A[i][j]
                I_chromaB[i][j] = D[i][j]*Z_B[i][j] + (one_minus_d[i][j])*I_B[i][j]
            else :
                I_lum[i][j] = I_L[i][j]
                I_chromaA[i][j] = I_A[i][j]
                I_chromaB[i][j] = I_B[i][j]

    
    I_lum = I_lum * 100
    I_chromaA = (I_chromaA * 255) - 128
    I_chromaB = (I_chromaB * 255) - 128
    
    # Merge the final output
    output_lab = cv.merge([I_lum,I_chromaA,I_chromaB])
    
    # cv.imwrite('./result/{}/{}/outputLAB.png'.format(image_name,nbiterations), output_lab)
    
    # Convert to RGB
    output = cv.cvtColor(output_lab, cv.COLOR_LAB2BGR)
    
    cv.imshow('float01', output) 
    cv.waitKey(0)  
    cv2.destroyAllWindows()  
    
    # Write images
    output = output * 255
    cv.imshow('Weathering', output) 
    cv.waitKey(0)  
    cv2.destroyAllWindows()  
    cv.imwrite('./result/{}/{}/output.png'.format(image_name,nbiterations), output)

    
    logger.info("Weathering Done !")
    
    return output


#====================================================================
#====================================================================
# WEATHERING - Video Output
#====================================================================
#====================================================================

@app.command()
def WeatheringVideo(path: str, nbiterations: int):
    
    # Read input image
    #path = "./input/moss.jpg"
    image = cv.imread(path)
    
    # Prepare image name and size for later
    path_split = path.split("/")
    size_split = len(path_split)
    image_name_full = path_split[size_split-1] # ex : moss.jpg
    name_split = image_name_full.split(".")
    image_name = name_split[0] # ex : moss
    size_x, size_y = image.shape[:2]
    
    #====================================================================
    # (a) User Input
    # The user picks pixel from most weathered region and from non-weathered region.
    #====================================================================

    logger.info("(a) Asking for User Input.")
    
    # The user selects most weathered region and least weathered region
    ROI = userInput(image,logger)
    
    
    #====================================================================
    # (b) Weathering Degree Map
    # We compute a weathered degree map based on Radial Basis Functions (RBFs) 
    # to handle wide color distributions over objects.
    #====================================================================
    
    logger.info("(b) Computing Weathering Degree Map ...")
    
    # Convert from RGB to Lab colorspace 
    image_lab = cv.cvtColor(image, cv.COLOR_BGR2LAB)
    
    # Prepare omega set : a set of pixels within the user-specified scribbles.
    omega = makeOmegaSet(image_lab, ROI)
    
    # Solve optimization problem : (eq. 1 from paper) + Compute the degree of each pixel : (eq. 2 from paper)
    degreeMap = computeDegreeMap(omega, image_lab, logger)
    
    # Segment the image with a threshold
    segmentation = computeSegmentation(degreeMap, image_name)
    
    logger.info("Weathering Degree Map done.")
    
    
    #====================================================================
    # (c) Weathering Exemplar
    # We compute an exemplar and we upscale it into a texture.
    #====================================================================
    
    logger.info("(c) Computing Weathering Exemplar and Texture ...")
    
    if (os.path.isfile("./result/{}/texture.png".format(image_name))):
        logger.info("Texture already computed.")
        texture = cv.imread("./result/{}/texture.png".format(image_name))
    else :
        # Compute the exemplar by asking the user to select most weathered area
        exemplar = computeExemplar(image, logger)
            
        # Upscale the exemplar into a texture by quilting
        logger.info("... Computing Texture (please wait a few minute) ...")
        texture = quilting(exemplar, size_x, size_y)
            
        # Write images
        cv.imwrite('./result/{}/texture.png'.format(image_name), texture)
    
    logger.info("Texture done.")
    
    
    #====================================================================
    # (d) Shading
    # We compute a shading map :
    #====================================================================
    
    logger.info("(d) Computing Shadow Map ...")
    
    # Compute the shadow map using the image,the weathering degree map and the binary segmented degree map
    shadowMap = computeShadingMapTest(image, degreeMap, segmentation)
    
    logger.info("Shadow Map Generated.")
    
    
    #====================================================================
    # (e) Updated Weathering Degree Map
    # Once both are computed, we update the weathering degree map.
    #====================================================================
    
    logger.info("(e) Updating All Weathering Degree Maps ...")
    
    degreeMapList = []
    for i in range(0, nbiterations+1) :
        degreeMapUpdated = updateDegreeMap(degreeMap, segmentation, i)
        degreeMapList.append(degreeMapUpdated)
    
    logger.info("Degree Maps updated.")
    
    
    #====================================================================
    # (f) Output
    # We spread patches from the exemplar to input image according to new 
    # weathering degree map and shadow map
    #====================================================================
    
    logger.info("(f) Computing the final video output ...")
    
    ### Prepare elements for computation (eq. (5) and (6) from the paper)
    
    # Prepare Input Image I
    image = np.float32(image) / 255  
    image_lab = cv.cvtColor(image, cv.COLOR_BGR2LAB)
    # cv.imwrite('./result/{}/{}/imageLAB.png'.format(image_name,nbiterations), image_lab)
    I_L, I_A, I_B = cv.split(image_lab)

    I_L = I_L / 100
    I_L = I_L[:,:, np.newaxis]
    I_A = (I_A + 128) / 255
    I_A = I_A[:,:, np.newaxis]
    I_B = (I_B + 128) / 255
    I_B = I_B[:,:, np.newaxis]

    # Prepare Weathering Texture Z
    texture = np.float32(texture) / 255
    texture_lab = cv.cvtColor(texture, cv.COLOR_BGR2LAB)
    
    Z_L, Z_A, Z_B = cv.split(texture_lab)
    Z_L = Z_L / 100
    Z_L = Z_L[:,:, np.newaxis]
    Z_A = (Z_A + 128) / 255
    Z_A = Z_A[:,:, np.newaxis]
    Z_B = (Z_B + 128) / 255
    Z_B = Z_B[:,:, np.newaxis]
    
    # Shading Map S
    S = shadowMap.copy()
    
    # Updated Degree Map D : D' = D if D > sigma, else D = 0
    DList = []
    for i in range(0, nbiterations+1) :
        D = degreeMapList[i].copy()
        sigma = 0.7 # cf paper
        D[D <= sigma] = 0
        DList.append(D)
        
    OneMinusDList = []        
    for i in range(0, nbiterations+1) :
        one_minus_d = np.subtract(1.0, DList[i])
        OneMinusDList.append(one_minus_d)

    
    # Compute all images
    WeatheringList = []
    
    for index in range(0, nbiterations+1) :
        I_lum = I_L.copy()
        I_chromaA = I_A.copy()
        I_chromaB = I_B.copy()
        D = DList[index]
        one_minus_d = OneMinusDList[index]
        # Eq. (5) and (6) from the paper  
        for i in range(0, size_x) :
            for j in range(0, size_y) :
                if (segmentation[i][j]==0) :
                    I_lum[i][j] = (D[i][j]*Z_L[i][j] + (one_minus_d[i][j])*I_L[i][j]) * S[i][j]
                    I_chromaA[i][j] = D[i][j]*Z_A[i][j] + (one_minus_d[i][j])*I_A[i][j]
                    I_chromaB[i][j] = D[i][j]*Z_B[i][j] + (one_minus_d[i][j])*I_B[i][j]
                else :
                    I_lum[i][j] = I_L[i][j]
                    I_chromaA[i][j] = I_A[i][j]
                    I_chromaB[i][j] = I_B[i][j]

        I_lum = I_lum * 100
        I_chromaA = (I_chromaA * 255) - 128
        I_chromaB = (I_chromaB * 255) - 128
    
        # Merge the final output
        output_lab = cv.merge([I_lum,I_chromaA,I_chromaB])
    
        # Convert to RGB
        output = cv.cvtColor(output_lab, cv.COLOR_LAB2BGR)
    
        # Write images 
        output = output * 255  
        output = output.astype('uint8')    
        WeatheringList.append(output)

    # Computing Video
    size = (size_y,size_x)
    out = cv2.VideoWriter('./result/{}/video_{}iter.mp4'.format(image_name,nbiterations), cv2.VideoWriter_fourcc(*'MP4V'), 15, size)
     
    for i in range(0, nbiterations+1) :
        out.write(WeatheringList[i])
    out.release()
    
    logger.info("Weathering Video Done !")
    
    return output

    
#===============================================
def main():
    app()

#===============================================
if __name__ == "__main__":
    main()

#===============================================
# How to run me
# python WEATHERING.py weathering ./input/moss.jpg 50

# Commande Spyder
# runfile('WEATHERING.py',args='weathering ./input/moss.jpg 50')
    


