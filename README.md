# WEATHERING : A Python reimplementation of "Single Image Weathering via Exemplar Propagation"

By Caroline PINTE and Bastien DANIEL

## Introduction

This project is a student image processing assignment, with the goal of reimplementing in Python the weathering method described in the article "Single Image Weathering via Exemplar Propagation" by Satoshi Iizuka, Yuki Endo,  Yoshihiro Kanamori and Jun Mitani.

A project report explaining our approach in detail is available in PDF version at the root of this repository.

## Contents
1. [Requirements](#requirements)
2. [Usage](#usage)
3. [Results](#results)
4. [Citation](#citing-weathering)

## Requirements

To run this project, you will need a python 3.6 environment with these installs:

* `pip install numpy`
* `pip install opencv-python`
* `pip install typer`
* `pip install logging`
* `pip install colorlog`

At the time of the project (Jan. 2021), the library versions are:

- Numpy: 1.19.4
- OpenCV: 4.5.1.48
- Typer: 0.3.2
- Logging: 0.4.9.6
- Colorlog: 4.6.2

## Usage

To run our method, please use the following commands depending of the output needed:

### Usage for an image output (prepare to wait a few minutes):

```console
[yourpath]/python [yourpath]/WEATHERING.py weathering [OPTIONS] PATH NBITERATIONS
```

**Arguments**:

* `PATH`: Path to input image.  [required]
* `NBITERATIONS`: Number of iterations needed.  [required]

**Options**:

* `--help`: Show this message and exit.

### Usage for a video output (prepare to wait for a longer time):

```console
[yourpath]/python [yourpath]/WEATHERING.py weatheringvideo [OPTIONS] PATH NBITERATIONS
```

**Arguments**:

* `PATH`: Path to input image.  [required]
* `NBITERATIONS`: Number of iterations needed.  [required]

**Options**:

* `--help`: Show this message and exit.

## Results

### Image outputs:

<img src="result/moss/result-moss.png" width="500">

<img src="result/house/result-house.png" width="500">

<img src="result/flowers/result-flowers.png" width="500">

<img src="result/forest/result-forest.png" width="500">

<img src="result/door/result-door.png" width="500">

### Video outputs (until 200 iterations):

Click on the gifs in order to be redirected to Youtube.

[![moss video](result/moss/result-moss.gif)](https://youtu.be/fYYpp4jdsZc)

[![house video](result/house/result-house.gif)](https://youtu.be/Q0NY18yNyWg)

[![flowers video](result/flowers/result-flowers.gif)](https://youtu.be/as2nPjCZ0ME)

[![forest video](result/forest/result-forest.gif)](https://youtu.be/Cs262mKFCG4)

[![door video](result/door/result-door.gif)](https://youtu.be/TKRXFVX5XTc)

## Citing WEATHERING

If you find this method useful, please consider citing:

```
@inproceedings{iizuka2016single,
  title={Single image weathering via exemplar propagation},
  author={Iizuka, Satoshi and Endo, Yuki and Kanamori, Yoshihiro and Mitani, Jun},
  booktitle={Computer Graphics Forum},
  volume={35},
  number={2},
  pages={501--509},
  year={2016},
  organization={Wiley Online Library}
}
```
